<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    {{-- <link rel="stylesheet" href="/css/app.css"> --}}
   <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <!------ Include the above in your HEAD tag ---------->

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" />

    <title>Document</title>
    <style>
    .parent{
        display:flex;
        flex-direction:row;
    }
    /* .container{
        padding:50px;
    } */
    
    .child{

        display:flex;
        align-items:center;
        padding:5px;
        padding-left:40px;
        width:100%;
        padding-right:40px;
    }

    .cover-img{
        background: url(pexels-photo-1498926.jpg);
        border: 2px solid #000000;
        max-width:80%;
        box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
        border-radius: 27px;
    }
    

h3.h3{text-align:center;margin:1em;text-transform:capitalize;font-size:1.7em;}
.product-grid6,.product-grid6 .product-image6{overflow:hidden}
.product-grid6{font-family:'Open Sans',sans-serif;text-align:center;position:relative;transition:all .5s ease 0s}
.product-grid6:hover{box-shadow:0 0 10px rgba(0,0,0,.3)}
.product-grid6 .product-image6 a{display:block}
.product-grid6 .product-image6 img{width:100%;height: 300px;transition:all .5s ease 0s}
.product-grid6:hover .product-image6 img{transform:scale(1.1)}
.product-grid6 .product-content{padding:12px 12px 15px;transition:all .5s ease 0s}
.product-grid6:hover .product-content{opacity:0}
.product-grid6 .title{font-size:20px;font-weight:600;text-transform:capitalize;margin:0 0 10px;transition:all .3s ease 0s}
.product-grid6 .title a{color:#000}
.product-grid6 .title a:hover{color:#2e86de}
.product-grid6 .price{font-size:18px;font-weight:600;color:#2e86de}
.product-grid6 .price span{color:#999;font-size:15px;font-weight:400;text-decoration:line-through;margin-left:7px;display:inline-block}
.product-grid6 .social{background-color:#fff;width:100%;padding:0;margin:0;list-style:none;opacity:0;transform:translateX(-50%);position:absolute;bottom:-50%;left:50%;z-index:1;transition:all .5s ease 0s}
.product-grid6:hover .social{opacity:1;bottom:20px}
.product-grid6 .social li{display:inline-block}
.product-grid6 .social li a{color:#909090;font-size:16px;line-height:45px;text-align:center;height:45px;width:45px;margin:0 7px;border:1px solid #909090;border-radius:50px;display:block;position:relative;transition:all .3s ease-in-out}
.product-grid6 .social li a:hover{color:#fff;background-color:#2e86de;width:80px}
.product-grid6 .social li a:after,.product-grid6 .social li a:before{content:attr(data-tip);color:#fff;background-color:#2e86de;font-size:12px;letter-spacing:1px;line-height:20px;padding:1px 5px;border-radius:5px;white-space:nowrap;opacity:0;transform:translateX(-50%);position:absolute;left:50%;top:-30px}
.product-grid6 .social li a:after{content:'';height:15px;width:15px;border-radius:0;transform:translateX(-50%) rotate(45deg);top:-20px;z-index:-1}
.product-grid6 .social li a:hover:after,.product-grid6 .social li a:hover:before{opacity:1}
@media only screen and (max-width:990px){.product-grid6{margin-bottom:30px}
}
.dropdown{
    padding-left:1rem;
}
.is-active{
    border-style: solid;
  border-width: 1px;
}
}
.dropdown-item{
    font-size:3em !important;
    color:red !important;
    background-color: transparent !important;
    border:0 !important;
    -webkit-appearance: none;
}

.algolia-autocomplete {
  width: 100%;
}
.algolia-autocomplete .aa-input, .algolia-autocomplete .aa-hint {
  width: 100%;
}
.algolia-autocomplete .aa-hint {
  color: #999;
}
.algolia-autocomplete .aa-dropdown-menu {
  width: 100%;
  background-color: #fff;
  border: 1px solid #999;
  border-top: none;
}
.algolia-autocomplete .aa-dropdown-menu .aa-suggestion {
  cursor: pointer;
  padding: 5px 4px;
}
.algolia-autocomplete .aa-dropdown-menu .aa-suggestion.aa-cursor {
  background-color: #B2D7FF;
}
.algolia-autocomplete .aa-dropdown-menu .aa-suggestion em {
  font-weight: bold;
  font-style: normal;
}



    </style>

    <script>
     function codeAddress() {
            var categoryid = {{$category->id}};
            var sizeid = {{$size}};

            document.getElementById('category'+categoryid).className += " is-active";
            document.getElementById('size'+sizeid).className += " is-active";


        }
        window.onload = codeAddress;
    </script>
</head>
<body>
    <script type="text/javascript">

        function addMark(a){
            var lastChar = a[a.length -1];

            if(lastChar == "?" || lastChar == "&"){
                a = a;
            }

            else{
                a = a + "?";
            }

            return a;
        }

        function link(a){
            // let a = event.target; Better way using Using data attributes
            var id = a.getAttribute("value");
            var name = a.getAttribute("name");
            var link =  window.location.href;
            link = addMark(link);
            var regex =name+'=[0-9]*';
            var RegExx = RegExp(regex);

            if(link.includes(name)){
            link = link.replace(RegExx, name + "=" + id);
            }

            else{
            link = link + name + "=" + id + "&";
            }

            a.setAttribute('href', link);
        }
        </script>

    <div class="parent">
        <div class="child d-flex justify-content-center" style="flex:1;">
      
            <div class="dropdown ">
                @foreach ($categories as $Vcategory)
            <a class="dropdown-item" id="category{{$Vcategory->id}}" onclick="link(this)" name="category_id" value="{{$Vcategory->id}}" href="#">{{$Vcategory->name}}</a>
                @endforeach       
            </div>
            
            <div class="dropdown ">
                @foreach ($sizes as $size)
                    <a class="dropdown-item" id="size{{$size->id}}" onclick="link(this)" value="{{$size->id}}" name="size_id"  href="#">{{$size->name}}</a>
                @endforeach
            </div>

        </div>
        <a href="/"><div class="child" style="flex:2;"><img class="cover-img" src="{{asset('img/cover.jpeg')}}" style="width:inherit;height:inherit;" alt=""></div></a>
    </div>

    <div class="container">


        

            <form class="bd-search d-flex align-items-center">
                    <span class="algolia-autocomplete algolia-autocomplete-left" style="position: relative; display: inline-block; direction: ltr;">
                        <input type="search" class="form-control ds-input" id="search" placeholder="Search..." autocomplete="off" data-docs-version="4.3" spellcheck="false" role="combobox" aria-autocomplete="list" aria-expanded="false" aria-owns="algolia-autocomplete-listbox-0" dir="auto" style="position: relative; vertical-align: top;">
                        <pre aria-hidden="true"  style="position: absolute; visibility: hidden; white-space: pre; font-family: -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, &quot;Noto Sans&quot;, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;, &quot;Noto Color Emoji&quot;; font-size: 16px; font-style: normal; font-variant: normal; font-weight: 400; word-spacing: 0px; letter-spacing: normal; text-indent: 0px; text-rendering: auto; text-transform: none;">
                        
                        d
                    
                    </pre>
                  
                    <span class="ds-dropdown-menu ds-with-1" role="listbox" id="algolia-autocomplete-listbox-0" style="position: absolute; top: 100%; z-index: 100; left: 0px; right: auto; display: block;"><div class="ds-dataset-1"><span class="ds-suggestions" style="display: block;"><div class="ds-suggestion ds-cursor" role="option" id="option-3674947" aria-selected="true">
                            <a class="algolia-docsearch-suggestion
                              algolia-docsearch-suggestion__main
                              algolia-docsearch-suggestion__secondary
                              " aria-label="Link to the result" href="https://getbootstrap.com/docs/4.3/layout/utilities-for-layout/" style="white-space: normal;">
                              
                              <div class="algolia-docsearch-suggestion--wrapper">
                         
                                  
                                </div>
                              </div>
                            </a>
                            </div></span>
                          

                    <button class="btn btn-link bd-search-docs-toggle d-md-none p-0 ml-3" type="button" data-toggle="collapse" data-target="#bd-docs-nav" aria-controls="bd-docs-nav" aria-expanded="false" aria-label="Toggle docs navigation"><svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 30 30" role="img" focusable="false"><title>Menu</title><path stroke="currentColor" stroke-linecap="round" stroke-miterlimit="10" stroke-width="2" d="M4 7h22M4 15h22M4 23h22"></path></svg></button>
                  </form>
        
       <h3 class="h3">{{$category->name}} Clothes</h3>
    <div class="row" id="bodyy">

        @foreach ($items as $item)
        
        <div class="col-md-3 col-sm-6">
            <div class="product-grid6">
                <div class="product-image6">
                    <a href="/item/{{$item->id}}">
                        <img class="pic-1" src="{{$item->url}}">
                    </a>
                </div>
                <div class="product-content">
                    <h3 class="title"><a href="#">{{$item->name}}</a></h3>
                    <div class="price">${{$item->price}}</div>
                </div>
                <ul class="social">
                    <li><a href="" data-tip="Quick View"><i class="fa fa-search"></i></a></li>
                    <li><a href="" data-tip="Add to Wishlist"><i class="fa fa-shopping-bag"></i></a></li>
                    <li><a href="" data-tip="Add to Cart"><i class="fa fa-shopping-cart"></i></a></li>
                </ul>
            </div>
        </div>
        @endforeach
    </div>

    {{ $items->links() }}
    </div>
  




    <script>
    
    $(document).ready(function(){
    
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    
      
               $("#search").keyup(function(){
                   var str=  $("#search").val();
                   if(str == "") {
                           $( "#bodyy" ).html("no search"); 
                   }else {
    
                    $.ajax({
                        url: ("{{ url('/search') }}"),
                        type:"GET",
                        data: {"id": str},
                        success:function(data){
                            // console.log(data);
                            $( "#bodyy" ).html( data );  
                            // alert(data);
                        },error:function(){ 
                            alert("error!!!!");
                        }
                    }); //end of ajax
    
                   }
               });  
            }); 
        </script>
    <script src="/js/app.js"></script>
</body>
</html>