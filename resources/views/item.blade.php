<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <style>
    body{
    font-family: 'Roboto Condensed', sans-serif;
    background-color: #f5f5f5
}
.hedding{
	font-size: 20px;
	color:#ab8181`;
}
.main-section{
	position: absolute;
	left:50%;
	right:50%;
	transform: translate(-50%,5%);
}
.left-side-product-box img{
	width: 100%;
}
.left-side-product-box .sub-img img{
	margin-top:5px;
	width:83px;
	height:100px;
}
.right-side-pro-detail span{
	font-size:15px;
}
.right-side-pro-detail p{
	font-size:25px;
	color:#a1a1a1;
}
.right-side-pro-detail .price-pro{
	color:#E45641;
}
.right-side-pro-detail .tag-section{
	font-size:18px;
	color:#5D4C46;
}
.pro-box-section .pro-box img{
	width: 100%;
	height:200px;
}
@media (min-width:360px) and (max-width:640px) {
	.pro-box-section .pro-box img{
		height:auto;
	}
}
    </style>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>
    <div class="container">
        <div class="col-lg-8 border p-3 main-section bg-white">
        <a href="{{url()->previous()}}">Back</a>
            <div class="row hedding m-0 pl-3 pt-0 pb-3">
                Details

            </div>
            <div class="row m-0">
                <div class="col-lg-4 left-side-product-box pb-3">
                    <img src="
                    {{$item->url}}
                    " class="border p-3">
                    
                </div>
                <div class="col-lg-8">
                    <div class="right-side-pro-detail border p-3 m-0">
                        <div class="row">
                            <div class="col-lg-12">
                                
                                <p class="m-0 p-0">{{$item->name}}</p>
                            </div>
                            <div class="col-lg-12">
                                <p class="m-0 p-0 price-pro">${{$item->price}}</p>
                                <hr class="p-0 m-0">
                            </div>
                            <div class="col-lg-12 pt-2">
                                <h5>Product Detail</h5>
                                <span>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                quis nostrud exercitation ullamco laboris.</span><hr class="m-0 pt-2 mt-2">
                            </div>
                            <div class="col-lg-12">
                                <p class="tag-section"><strong>Category : </strong><a href="">{{$item->category->name}}</a></p>
                            </div>
                            <div class="col-lg-12">
                                <p class="tag-section"><strong>Size : </strong><a href="">{{$item->size->name}}</a></p>
                            </div>
                            <div class="col-lg-12 mt-3">
                                <div class="row">
                                    <div class="col-lg-6 pb-2">
                                        <a href="#" class="btn btn-light w-100">Add To Cart</a>
                                    </div>
                                    <div class="col-lg-6">
                                        <a href="#" class="btn btn-light w-100">Shop Now</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            
        </div>
    </div>
    </body>
</html>