
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <title>Document</title>
    <script
  src="https://code.jquery.com/jquery-3.3.1.js"
  integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
  crossorigin="anonymous"></script>


</head>
<body>
    
    
<!-- search box container starts  -->
@csrf
<div class="search">
        <h3 class="text-center title-color">Ajax Live Search Table Demo</h3>
        <p>&nbsp;</p>
        <div class="row">
            <div class="col-lg-10 col-lg-offset-1">
                <div class="input-group">
                    <span class="input-group-addon" style="color: white; background-color: rgb(124,77,255);">BLOG SEARCH</span>
                    <input type="text" autocomplete="off" id="search" class="form-control input-lg" placeholder="Enter Blog Title Here">
                </div>
            </div>
        </div>   
    </div>  
<!-- search box container ends  -->
<div id="txtHint" class="title-color" style="padding-top:50px; text-align:center;" ><b>Blogs information will be listed here...</b></div>

<script>
    
        $(document).ready(function(){
        
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

  
           $("#search").keyup(function(){
               var str=  $("#search").val();
               if(str == "") {
                       $( "#txtHint" ).html("<b>Blogs information will be listed here...</b>"); 
               }else {

                $.ajax({
                    url: ("{{ url('search') }}"),
                    type:"GET",
                    data: {"id": str},
                    success:function(data){
                        // console.log(data);
                        $( "#txtHint" ).html( data );  
                        // alert(data);
                    },error:function(){ 
                        alert("error!!!!");
                    }
                }); //end of ajax

               }
           });  
        }); 
    </script>

<script
src="https://code.jquery.com/jquery-3.3.1.js"
integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
crossorigin="anonymous"></script>
</body>
</html>
