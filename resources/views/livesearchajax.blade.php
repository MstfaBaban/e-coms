<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

@csrf
<?php

if(!empty($items))  
{ 
    
    $count = 1;

                  
    foreach ($items as $item)    
    {   
 
    // $show = url('item/'.$item->slug);


?>

        
<div class="col-md-3 col-sm-6">
    <div class="product-grid6">
        <div class="product-image6">
            <a href="/item/{{$item->id}}">
                <img class="pic-1" src="{{$item->url}}">
            </a>
        </div>
        <div class="product-content">
            <h3 class="title"><a href="#">{{$item->name}}</a></h3>
            <div class="price">${{$item->price}}</div>
        </div>
        <ul class="social">
            <li><a href="" data-tip="Quick View"><i class="fa fa-search"></i></a></li>
            <li><a href="" data-tip="Add to Wishlist"><i class="fa fa-shopping-bag"></i></a></li>
            <li><a href="" data-tip="Add to Cart"><i class="fa fa-shopping-cart"></i></a></li>
        </ul>
    </div>
</div>


<?php    
                
    }  

 }  
 
 else  
 {  
    echo 'Data Not Found';  
 } 
 ?>  
     
</body>
</html>