<?php

namespace App\Http\Controllers;

use App\Category;
use App\Item;
use App\Size;

use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */

    public function show(Request $request)
    {
    
        $items = Item::get();
        $size=0;
        $category_id = 1;
        $category = Category::where('id',$category_id)->first();

        if(isset($request->category_id)){

            $category_id = $request->category_id;
            $items = Item::where('category_id',$category_id)->paginate(12);
            $category = Category::where('id',$category_id)->first();
            $sizes = Size::where('category_id',$category_id)->get();

            if(isset($request->size_id)){
                $items = Item::where('size_id',$request->size_id)->where('category_id',$category_id)->paginate(12);
                $size =$request->size_id;
                $sizes = Size::where('category_id',$category_id)->get();

            }
        }
               
        else{
            $items = Item::where('featured',1)->paginate(12);
            $category = Category::where('id',$category_id)->first();
            $category->name = 'featured';
            $sizes = collect(new Size);               
            $category->id = 'featured';
        }

        $categories = Category::get();
        

        return view('welcome',compact('category','categories','items','sizes','size'));
    }

    public function showsize($id,$sid)
    {

        $categories = Category::all();
        $category = Category::where('id',$id)->first();
        $sizes = Size::where('category_id',$id)->get();

        if($category->id == 1){
            $items = Item::paginate(12);
        }

        else{
            $items = Item::where('category_id',$category->id)->where('size_id',$sid)->paginate(12);
        }

        return view('welcome',compact('category','categories','items','sizes'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        //
    }
}
