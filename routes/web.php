<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     $categories = App\Category::all();
//     $category = App\Category::where('id',1)->first();
//     $sizes = App\Size::where('category_id',1)->get();
//     // dd($sizes);
//     $items = App\Item::where('category_id',1)->paginate(12);
//     return view('welcome',compact('categories','items','category','sizes'));
// });
Route::get('/','CategoryController@show');
Route::get('/item/{id}','ItemController@show');
Route::get('search','ItemController@liveSearch');        
// Route::get('{id}/{sid}','CategoryController@showsize');

